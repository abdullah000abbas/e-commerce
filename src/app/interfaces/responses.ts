export interface CategoryResponse {
    id: number;
    label: String;
  }

  export interface ProductResponse {
    id: number;
    name: string;
    image: string;
    category: CategoryResponse;
    price: number;
    made_In: string;
  }