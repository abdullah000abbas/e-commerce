export interface CreateProduct {
    name: string; 
    price: number;
    categoryId: number;  
    made_In: string;
  }
  export interface updated_Product_Request{
    id:number;
    name: string; 
    price: number;
    made_In: string;
  }
  export interface CreateCategory {
    label: String;
  }