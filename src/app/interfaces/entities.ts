export interface Category {
    id: number;
    label: string;
}
export interface Product {
    id: number;
    name: string;
    image: string;
    category: Category;
    price: number;
    made_In: string;
}