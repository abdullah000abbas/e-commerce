import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Product, Category } from '../interfaces/entities';
import { CreateProduct, CreateCategory,updated_Product_Request } from '../interfaces/requests';
import { CategoryResponse, ProductResponse } from '../interfaces/responses';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  // BASE_URL='http://localhost:5090/';

  public categories = [
    { id: 1, label: 'clothes' },
    { id: 2, label: 'beaty' },
    { id: 3, label: 'shoes' },
    { id: 4, label: 'electronics' }
  ];
  public products = [
    { id: 1, name: 'Red T-Shirt', price: 15, made_In: 'China', category: this.categories[0].label },
    { id: 2, name: 'Red Jacket', price: 25, made_In: 'China', category: this.categories[0].label },
    { id: 3, name: 'Pink rouge', price: 5, made_In: 'Syria', category: this.categories[1].label },
    { id: 4, name: 'Black Iliner', price: 5, made_In: 'Lebanon', category: this.categories[1].label },
    { id: 5, name: 'Cleaner', price: 50, made_In: 'Europe', category: this.categories[3].label },

  ];


  constructor(/*private http: HttpClient*/) { }


  /*
  
  //get product by id
    getProducts(id:any){
  
      const url = this.BASE_URL + 'api/user/get/{id}';
      const headers = new HttpHeaders({
        'Content-Type': 'application/json'
      });
    
      return  this.http.get(url, { headers: headers });
    }
  
    //get all products
    async getItems(): Promise<Product[]> {
  
      const url = this.BASE_URL + 'api/item/get';
      
      const headers = new HttpHeaders({
        'Content-Type': 'application/json'
      });
      
      let response = await this.http.get<any>(url, { headers: headers }).toPromise();
      const products: Product[] = response.map((item: ProductResponse) => ({
        id: item.id,
        name: item.name,
        price:item.price,
        category: {
          id: item.category.id,
          label: item.category.label
        },
        made_In:item.made_In
  
      }));
      
      return products;
    }
    */
      ////edit product
      async updateProduct(updatedproduct: updated_Product_Request ): Promise<void> {
      /*  
           const url = this.BASE_URL+'api/order/update/${id}';
      
          const headers = new HttpHeaders({
            'Content-Type': 'application/json'
          });
          console.log(updatedproduct);
          await this.http.patch(url,updatedproduct);
       */
      console.log('product updated')   
      }
  
  
  
      //delete product
      async deleteProduct(id: any): Promise<void> {
       /* console.log("order deleted")
         const url =  this.BASE_URL+'api/order/delete/${id}';
      
        const headers = new HttpHeaders({
          'Content-Type': 'application/json'
        });
        await this.http.delete(url,id);
        alert("deleted order with id ${id}");
      */
        console.log('product deleted')
    
      }
  
  
  
      ///////////
      //category services
  
      //get categories
      /*
      async getCategories(): Promise<Category[]> {
  
        const url = this.BASE_URL + 'api/category/get';
        
        const headers = new HttpHeaders({
          'Content-Type': 'application/json'
        });
        
        let response = await this.http.get<any>(url, { headers: headers }).toPromise();
        const categories: Category[] = response.map((cat: CategoryResponse) => ({
          id: cat.id,
          label: cat.label
        }));
        
        return categories;
      }
  
  
      */


}
