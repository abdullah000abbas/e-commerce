import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent{
  contactForm: FormGroup;
  constructor(private fb: FormBuilder){
    this.contactForm = this.fb.group({
      name: this.fb.control('', Validators.required),
      email: this.fb.control('', [Validators.required, Validators.email]),
      topic: this.fb.control('', Validators.required),
      message: this.fb.control('', [Validators.required, Validators.min(50)])
    })
  }

  Send(){
    if(this.contactForm.valid){
      console.log(this.contactForm.value);
    }
  }
}
