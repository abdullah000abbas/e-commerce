import { Component , ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProductsService } from 'src/app/services/products.service';
import { updated_Product_Request } from 'src/app/interfaces/requests';
@Component({
  selector: 'app-edit-product-popup',
  templateUrl: './edit-product-popup.component.html',
  styleUrls: ['./edit-product-popup.component.css']
})
export class EditProductPopupComponent implements OnInit{
  form!: FormGroup;
  @ViewChild('popup') popupRef!: ElementRef;
  productDetails: any;

  id=1;
  name=""; 
  price=0; 
  made_In="";
  category_Id=1;

  constructor(private service: ProductsService, private router: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public data: { modal: any }) { }


    ngOnInit(): void {
      console.log(this.data.modal);
      this.productDetails = this.data.modal
  
      this.form = new FormGroup({
        image: new FormControl(null),
        name: new FormControl(null, Validators.required),
        madein: new FormControl(null, Validators.required),
        price: new FormControl(null, Validators.required)
      })
  
    }

    onSubmit() {
      this.popupRef.nativeElement.hidden = true;

      const  updatedUser:updated_Product_Request={
        id:this.id,
        name:this.name,
        price:this.price,
        made_In:this.made_In
         } ;
         this.service.updateProduct(updatedUser);
        
      document.querySelector(`.content`)!.classList.remove(`opacity`)
    }
  
    selectedImage: any;
  
    handleFileInput(event: any): void {
      const file: File = event.target.files[0];
      const reader = new FileReader();
  
      reader.onload = (e) => {
        this.selectedImage = e.target?.result;
      };
  
      reader.readAsDataURL(file);
    }
}
