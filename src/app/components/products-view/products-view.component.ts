import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/services/products.service';
import { CreateProduct, updated_Product_Request } from 'src/app/interfaces/requests';
import { Product } from 'src/app/interfaces/entities';
import { MatDialog } from '@angular/material/dialog';
import { EditProductPopupComponent } from './edit-product-popup/edit-product-popup.component';

@Component({
  selector: 'app-products-view',
  templateUrl: './products-view.component.html',
  styleUrls: ['./products-view.component.css']
})
export class ProductsViewComponent implements OnInit {

  name = "";
  price = 0;
  made_In = "";
  category_Id = 1;
  opacity : boolean = false;
  product: any[] = [];

  showPopup : boolean = false;
  products : any;
  selectedProductID : number;
  categories : any;
  constructor(private product_service: ProductsService, private matDialog: MatDialog) { }
  ngOnInit(): void {
    this.categories = this.product_service.categories;
    this.products = this.product_service.products;
  }

  Popup(){
    this.showPopup = true;
  }
  closePopup(){
    this.showPopup = false;
  }
  editProduct(modal: any) {
    this.opacity = true;
    document.querySelector(`.content`)!.classList.add(`opacity`)
    const dialogRef = this.matDialog.open(EditProductPopupComponent, {
      disableClose: false,
      data: { modal }
    })
    dialogRef.afterClosed().subscribe(result => {
      this.opacity = false;
    });
  }


  deleteProduct(id: any) {
    if (confirm("Are you sure to delete this product?") == true) {
      this.product_service.deleteProduct(id)

      this.products = this.products.filter(x => x.id != id);
    }
  }
}
